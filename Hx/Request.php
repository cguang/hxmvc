<?php

namespace Hx;

/**
 * 请求类
 * Class Request
 * @package Hx
 */
class Request {

    /**
     * GET数据
     * @param bool $key
     * @return string|array
     */
    public static function get($key = true) {
        if ($key === true) {
            return $_GET;
        }
        return $_GET[$key];
    }

    /**
     * POST数据
     * @param bool|string $key
     * @return string|array
     */
    public static function post($key = true) {
        if ($key === true) {
            return $_POST;
        }
        return $_POST[$key];
    }

    /**
     * 数组获取请求数据
     * @param string $key
     * @return array
     */
    public static function request($key) {
        if (is_string($key)) {
            $key = explode(',', $key);
        }
        $return = array();
        foreach ($key as $k) {
            $return[$k] = $_REQUEST[$k];
        }
        return $return;
    }

    /**
     * 所有请求数据
     * @return array
     */
    public static function all() {
        return $_REQUEST;
    }

    /**
     * 判断是否有某个数据
     * @param string $key
     * @return bool
     */
    public static function has($key) {
        return isset($_REQUEST[$key]);
    }

    /**
     * 设置和获取Cookie
     * @param string $name
     * @param null|string $value
     * @param int $expire
     * @param string $path
     * @param null|string $domain
     * @return mixed
     */
    public static function cookie($name, $value = null, $expire = 0, $path = '/', $domain = null) {
        if ($value === null) {
            return $_COOKIE[$name];
        }
        setcookie($name, $value, time() + $expire, $path, $domain);
        $_COOKIE[$name] = $value;
        return $value;
    }

    //-----------------------------------

    /**
     * IP地址
     * @var string
     */
    public static $ip;

    /**
     * 客户端标识
     * @var string
     */
    public static $agent;

    /**
     * 请求方式
     * @var string
     */
    public static $method;

    /**
     * Restful请求方式
     * @var string
     */
    public static $restMethod;

    /**
     * 是否是Ajax请求
     * @var bool
     */
    public static $isAjax;

    /**
     * 顶级地址
     * @var string
     */
    public static $baseUrl;

    /**
     * 请求地址
     * @var string
     */
    public static $requestUrl;

    /**
     * 上一次访问地址
     * @var string
     */
    public static $refer;

    /**
     * 主机域名
     * @var string
     */
    public static $host;

    /**
     * 初始化请求数据
     */
    public static function initRequest() {
        self::$method = strtoupper($_SERVER['REQUEST_METHOD']);
        self::$agent = $_SERVER['HTTP_USER_AGENT'];
        self::$refer = $_SERVER['HTTP_REFERER'];
        self::$host = $_SERVER['HTTP_HOST'];
        self::$baseUrl = 'http://' . self::$host . '/';
        self::$requestUrl = 'http://' . self::$host . $_SERVER['PATH_INFO'];
        self::$isAjax = strtoupper($_SERVER['HTTP_X_REQUESTED_WITH']) == 'XMLHTTPREQUEST';
        self::$restMethod = isset($_SERVER['HTTP_REST_METHOD']) ? strtoupper($_SERVER['HTTP_REST_METHOD']) : self::$method;
        self::$ip = self::getIp();
    }

    /**
     * 获取IP
     * @return string
     */
    private static function getIp() {
        return $_SERVER['REMOTE_ADDR'];
    }

    /**
     * 获取HTTP头
     * @param string $name
     * @return mixed
     */
    public static function getHttp($name) {
        return $_SERVER[strtoupper('HTTP_' . $name)];
    }

    /**
     * 获取Server头
     * @param string $name
     * @return mixed
     */
    public static function getServer($name) {
        return $_SERVER[strtoupper($name)];
    }

    /**
     * 获取环境变量
     * @param string $name
     * @return string
     */
    public static function getEnv($name) {
        return getenv($name);
    }

}